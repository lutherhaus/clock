#include <cmath>

// port of https://github.com/thstielow/raspi-bme680-iaq/blob/main/bme680IAQ.py

float waterSatDensity(float temp) {
  return (6.112 * 100 * exp((17.62 * temp)/(243.12 + temp))) /
    (461.52 * (temp + 273.15));
}

float calc_comp_gas(float gas, float hum, float temp, float slope) {
  float rho_max = waterSatDensity(temp);
  float hum_abs = hum * 10.0 * rho_max;
  return gas * exp(slope * hum_abs);
}

float calc_hum_aq(float hum) {
  return 1.0 - pow(abs(50.0 - hum), 2.0) / 2500.0;
}

// vim: set sw=2 et:
