include <BOSL2/std.scad>

$fs = .4;
$fa = 1;

eps = 1/200;

/*
 * frontshelf
 */

module frontshelf() {
    move([209, 80]) xrot(90) color("pink")
        xscale(1.005) import("frontshelf.stl", convexity=5);
}

/*
 * backshelf
 */

// base plate thickness
base = 2 - eps;

psu = [44, 21];
hole_off = [7, 2.75];

// air sensor
bme680 = [16.1, 13.8];
bme680_off = [44, -22-eps];
ro = .25;

module nut() {
    tube(4.5, 3, 1.25, anchor=BOT);
}

module powersupply_mount() {
    rect_tube(6, psu+[2, 2], psu);
    rect_tube(4.5, psu+[eps, eps], psu-[2, 2]);
    move([psu[0]/2-hole_off[0], -psu[1]/2+hole_off[1]]) nut();
    move([-psu[0]/2+hole_off[0], psu[1]/2-hole_off[1]]) nut();
}

module sensor_mount() {
    diff()
        move([-1, 1])
            rect_tube(6, bme680+[2, 2], bme680-[2, 2], anchor=LEFT+BACK+BOT)
            position(BACK) fwd(3-ro)
            tag("remove") cuboid([bme680[0]+2*ro, 2, 6+eps], anchor=FWD);
    for (x=[2.5, bme680[0]-2.5])
        move([x, -11]) zcyl(4.5, 3, anchor=BOT);
}

module sensor_pins() {
    for (x=[2.5, bme680[0]-2.5])
        move([x, -11]) color("red") zcyl(8, .9, anchor=BOT);
}

module sensor_cutouts() {
    zrot(-90) {
        linear_extrude(5) offset(r=ro)
        import("BME680_Module_breadboard.svg");
}}

module holes() {
    move(bme680_off) up(base+4.5) sensor_cutouts();
    // ventilation
    for (x=[-2:1:2]) left(x*20) up(8) cuboid([2.5, 90, 12+eps], anchor=BOT);
    // mounting holes
    for (x=[-2:1:2])
        if (x!=0) move([x*30, 8, -eps]) cyl(3, 3, anchor=BOT);
}

module backshelf() {
    difference() {
        union() {
            move([209, 10, 40]) xrot(-90) color("green")
                xscale(1.005) import("backshelf.stl", convexity=5);
            move(bme680_off) up(base) sensor_mount();
        }
        holes();
    }
    move([0, 24.2, base]) powersupply_mount();
    move(bme680_off) up(base) sensor_pins();
}

difference() {
    union() {
        frontshelf();
        backshelf();
    }
}

// vim: set et sw=4:
