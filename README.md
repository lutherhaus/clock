Stage clock
===========

## How to build

1. Clone repo

2. Initialize Python3 venv:
	python3 -mvenv .

3. Install esphome:
	bin/pip install -U pillow esphome

4. Copy secrets.yaml.template to secrets.yaml and fill in secrets.

5. Compile clock firmware:
	bin/esphome build clock.yaml
